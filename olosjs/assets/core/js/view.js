//apenas para navegadores que não tem suporte para console.log
if (!console) {
    var console = window.console = {
        log: function () { }
    };
}


/* Configuração do Web Service Olos  */
//var addrs = { wsAgentCmd: "http://172.62.80.52/WsAgentControl_JSON/WsAgentCommandJSON.asmx", wsAgentEvt: "http://172.62.80.52/WsAgentControl_JSON/WsAgentEventJSON.asmx", wsMailingCmd: "http://172.62.80.52/WsMailingControl_JSON/WsMailingCommand.asmx", wsAgentConfigCmd: "" };
//var server = "http://191.209.125.51:8081";
var server = "187.51.62.170:8082";

var addrs = {
    wsAgentCmd: server + "/WsAgentControl/WsAgentCommandJSON.asmx",
    wsAgentEvt: server + "/WsAgentControl/WsAgentEventJSON.asmx",
    WsAgentCloud: server + "/WsAgentControl/WsCloudAgent.asmx", 
    wsMailingCmd: server + "/WsMailingControl/WsMailingCommand.asmx",
    wsIntegration: server + "/WsIntegration/WsIntegrationJSON.asmx",
    wsUserConfig: server + "/WSConfiguration/WSAgentConfigJSON.asmx",
    wsVoiceSupport: server + "/WsVoiceSupportIntegration/WsVoiceSupportIntegration.asmx"
};

/* Conectando no Web Service */
Olos.connect(addrs);

//Ativa o Log da API (obrigatorio)
//@param {int} logOpenWindow 0 - Para desabilitar a janela de logs. 1 - Para habilitar. (0 = mostra somente em tela sem janela)
//@param {String} logLevel Indica o nível de logque será uitlizado. Opcoes: NONE, ERROR, INFO, DEBUG
//NONE = só irá visulaizar os logs de Eventos e não os LOGS da API
Olos.setLogger(0, 'NONE');


/* Configurando os Logs */
var log = document.getElementById('log');
Olos.on('log', function (msgObj) {
    //console.log('%O', msgObj);
    updateLog(msgObj);
});

$(document).ready(function () {

    //Funcões de auxilio para tela de login  
    var startLoginAuth = function () {
        var login = $('#login').val();
        var passwd = $('#password').val();
        Olos.agentCommand.agentAuthentication(login, passwd);
    };

    var startLoginAuthPause = function () {
        var login = $('#login').val();
        var passwd = $('#password').val();
        Olos.agentCommand.agentAuthenticationWithPause(login, passwd, 'olos');
    };

    var startChatLoginAuth = function(){
        var login = $('#login').val();
        var passwd = $('#password').val();
        Olos.agentChannelCommand.agentAuthenticationChat(login, passwd);
    };

    var startChatLoginAuthPause = function () {
        var login = $('#login').val();
        var passwd = $('#password').val();
        Olos.agentChannelCommand.agentAuthenticationChatWithPause(login, passwd, '102');
    };

    /*  Inicio das chamadas de metodos da API que serão consumidos pelos botões e ações do CRM */
    $('#startLogin').on('click', function () {
         
        if ($('#login_pause').is(':checked') == true) {
            if ($('#login_chat').is(':checked') == true){
                startChatLoginAuthPause();
            } else {
                startLoginAuthPause();
            }
        } else {
            if ($('#login_chat').is(':checked') == true){
                startChatLoginAuth();
            } else {
                startLoginAuth();
                
            }
        }
    });

    $('#password').on('keyup', function (e) {
        if (e.which === 13) {
            startLoginAuth();
        }
    });

    $('#pause').on('click', function () {
        Olos.agentCommand.agentReasonRequest(100);
    });

    $('#pauseChat').on('click', function () {
        Olos.agentChannelCommand.agentReasonChatRequest(100);
    });

    $('#pause_coordinated').on('click', function () {
        Olos.agentCommand.coordinatedAgentPause(100, '');
    });

    $('#requestIdle').on('click', function () {
        Olos.agentCommand.agentIdleRequest();
    });

    $('#requestIdleChat').on('click', function () {
        Olos.agentChannelCommand.agentIdleChatRequest();
    });

    $('#disposition').on('click', function () {
        Olos.agentCommand.dispositionCall(100);
    });
    $('#dispositionCode').on('click', function () {
        Olos.agentCommand.dispositionCallByCode('106');
    });
    $('#dispositionCB').on('click', function () {
        //Olos.agentCommand.dispositionCallBack(107, '2013', '04', '11', '17', '30', '1122223333', 0);
        Olos.agentCommand.dispositionCallBack(102, '2016', '09', '29', '08', '30', '81996709452', false);
    });
    $('#dispositionCBCode').on('click', function () {
        Olos.agentCommand.dispositionCallbackByCode('105', '2013', '04', '11', '17', '30', '1122223333', 1);
    });
    $('#hangup').on('click', function () {
        Olos.agentCommand.hangupRequest();
    });
    $('#redial').on('click', function () {
        Olos.agentCommand.redialRequest();
    });
    $('#manualInit').on('click', function () {
        Olos.agentCommand.manualCallStateRequest();
    });
    $('#manualEnd').on('click', function () {
        Olos.agentCommand.endManualCallStateRequest();
    });
    $('#manualDial').on('click', function () {
        Olos.agentCommand.sendManualCallRequest('11', '22223333', 85);
    });
    $('#consulting').on('click', function () {
        Olos.agentCommand.consultingRequest('1@10.15.15.10', 0, '');
    });
    $('#transfer').on('click', function () {
        Olos.agentCommand.transferCallRequest();
    });
    $('#retrieve').on('click', function () {
        Olos.agentCommand.retrievesCall();
    });
    $('#insertPhone').on('click', function () {
        Olos.agentCommand.insertPhoneNumber('88', '44445555');
    });
    $('#invalidatePhone').on('click', function () {
        Olos.agentCommand.invalidatePhoneNumber('88', '44445555');
    });
    $('#updateMailing').on('click', function () {
        Olos.agentCommand.updateMailingData('<Nome>Joao da Silva</Nome><Endereco>Av Paulista, 0000</Endereco>');
    });
    $('#getMySupervisor').on('click', function () {
        Olos.agentCommand.getMySupervisor(48);
    });
    $('#retrieveSupervisioningMessages').on('click', function () {
        Olos.agentCommand.retrieveSupervisioningMessages(48, 0, '2019-08-01', '2019-08-16', false, true);
    });
    $('#sendMeSupervisor').on('click', function () {
        Olos.agentCommand.sendMessageToSupervisor(48, 49, "Mensagem de Teste para o Supervisor");
    });

    $('#pauseRecording').on('click', function () {
        Olos.agentCommand.pauseRecording(null,true, 0);
    });

    $('#resumeRecording').on('click', function () {
        Olos.agentCommand.resumeRecording(null, true);
    });


    
    $('#logout').on('click', function () {
        Olos.agentCommand.agentLogout();
    });

    $('#logout_coordinated').on('click', function () {
        Olos.agentCommand.coordinatedAgentLogout();
    });

    $('#getStatus').on('click', function () {
        var agentStatus = Olos.agentCommand.getAgentStatus();
        updateLog('AgentStatus: ' + agentStatus);
    });
    $('#listReasons').on('click', function () {
        Olos.agentCommand.listReasons();
    });

    $('#changeAgentCampaign').on('click', function () {
        var changed = Olos.userConfig.changeAgentCampaign('agente', '1', 7);
        updateLog('ChangeAgentCampaign: ' + changed);
    });
    $('#changeAgentSupervisor').on('click', function () {
        var changed = Olos.userConfig.changeAgentSupervisor('agente', 'edu');
        updateLog('ChangeAgentSupervisor: ' + changed);
    });
    $('#createAgent').on('click', function () {
        var created = Olos.userConfig.createAgent('AgenteA1', 'agentea1', 'olos', 1, 8);
        updateLog('CreateAgent: ' + created);
    });
    $('#getProfiles').on('click', function () {
        var profiles = Olos.userConfig.getUserProfiles();
        updateLog('Profiles: ' + profiles);
    });
    $('#getUserStatus').on('click', function () {
        var result = Olos.userConfig.getUserStatus('agente');
        updateLog('GetUserStatus: ' + result);
    });
    $('#setUserStatus').on('click', function () {
        var result = Olos.userConfig.setUserStatus('agente', false);
        updateLog('SetUserStatus: ' + result);
    });
    $('#getCampaignByUserLogin').on('click', function () {
        var result = Olos.userConfig.getCampaignByUserLogin('agente');
        updateLog('GetCampaignByUserLogin: ' + result);
    });
    $('#getCustomerByUserLogin').on('click', function () {
        var result = Olos.userConfig.getCustomerByUserLogin('agente');
        updateLog('GetCustomerByUserLogin: ' + result);
    });

    $('#limparLog').on('click', function () {
        clearLog();
    });

    $('#getVersion').on('click', function () {
        var result = Olos.getVersionAPI();
        updateLog('Versão da API: ' + JSON.stringify(result));
    });

    $('#agentsToConsulting').on('click', function () {
        Olos.integrationCommand.listAvailableOnlineAgentsByCompany(54);
    });

    $('#campaignsToConsulting').on('click', function () {
        Olos.integrationCommand.listCampaignsToConsulting(18);
    });

    $('#updateCallData').on('click', function () {

        if (typeof dadosCallData != 'undefined') {
            var calldataNew = prompt("Digite a informação adicional da CallData.");
            Olos.agentCommand.updateCallData(dadosCallData + calldataNew);
            updateLog('<strong>updateCallData sucesso</strong>: ');
        } else {
            alert('Não existe nenhuma ligação ativa para recuperar os dados originais');
        }
    });

    $('#sendPreviewCall').on('click', function () {
        Olos.agentCommand.SendPreviewCallRequest('11997333358');
    });


    /* Region: methods ChatAgent */
    $('#sendMessageChat').on('click', function() {
        var reqId = Olos.agentChannelCommand.sendMessage("Mensagem de Teste");
        updateLog('<strong>Send Message ReqID:' + JSON.stringify(reqId) + '</strong>: ');
    });

    $('#listReasonsChat').on('click', function () {
        Olos.agentChannelCommand.listChatPauseReasons();
    });

    $('#typing').on('click', function() {
        Olos.agentChannelCommand.typing();
        ///Olos.agentCommand.SendPreviewCallRequest('11997333358');
    });
    
    $('#currentDialogs').on('click', function() {
        Olos.agentChannelCommand.currentDialogsRequest();
        ///Olos.agentCommand.SendPreviewCallRequest('11997333358');
    });

    $('#dispositionChat').on('click', function() {
        Olos.agentChannelCommand.dispositionChat('01028c17df000000', 114, "", "", "", "", "", "", false);
    });
    
    $('#logout_chat').on('click', function () {
        Olos.agentChannelCommand.agentChannelLogout();
    });
    
    /* End Region: methods ChatAgent */

    $('#conferenceRequest').on('click', function () {
        Olos.agentCommand.conferenceRequest();
    });

    $('#stopConferenceRequest').on('click', function () {
        Olos.agentCommand.stopConferenceRequest();
    });


    $('#conferenceRequest').on('click', function () {
        Olos.agentCommand.conferenceRequest();
    });

    $('#stopConferenceRequest').on('click', function () {
        Olos.agentCommand.stopConferenceRequest();
    });

    $('#referCallRequest').on('click', function () {
        Olos.agentCommand.referCallRequest('1', '11997333358', '', '');
    });

});

/* Fim das chamadas dos metodos */


/* Inicio da criação de Eventos do Olos , para toda chamada de metodo da API, retona-se um evento de confirmação
 * Exemplo:
 *  
 * método: agentAuthentication =  irá retornar um evento do mesmo nome.
 *
 * Olos.agentCommand.on('agentAuthentication', function(agentId) {
 *
 *    if (agentId == 0) {
 *        alert('Agente ou Senha Incorretos!');
 *    } else {
 *        alert(ùsuario Logado);
 *    }
 * });
 *  
 */

var clearScreen = function() {
    $('#login-screen').hide();
        $('header').show();
        $('nav').show();
        $('#page-content').show();
        $('footer').show();
        $('#sub-last').addClass('init-status');
}

Olos.agentCommand.on('agentAuthentication', function (agentId) {
        clearScreen();
        //chamada para iniciar os eventos do chat
       Olos.agentChannelCommand.start(agentId);
});

Olos.agentCommand.on('agentAuthenticationFail', function (agentId) {
    clearScreen();
    updateLog('<strong>agentAuthentication fail:</strong> ' + JSON.stringify(agentId));
});

Olos.agentCommand.on('agentAuthenticationWithPause', function (agentId) {
    clearScreen();
          //chamada para iniciar o chat
        Olos.agentChannelCommand.start(agentId);
});

Olos.agentCommand.on('agentAuthenticationWithPauseFail', function (agentId) {
    clearScreen();
    updateLog('<strong>agentAuthenticationWithPause fail:</strong> ' + JSON.stringify(agentId));
});


Olos.agentChannelCommand.on('agentAuthenticationChat', function (agentId) {
        $('#login-screen').hide();
        $('header').show();
        $('nav').show();
        $('#page-content').show();
        $('footer').show();
        $('#sub-last').addClass('idle');
});



Olos.agentChannelCommand.on('agentAuthenticationChatFail', function (agentId) {
    clearScreen();
    updateLog('<strong>agentAuthenticationChat fail:</strong> ' + JSON.stringify(agentId));
});


Olos.agentChannelCommand.on('agentAuthenticationChatWithPause', function (agentId) {
    if (agentId > 0) {
        $('#login-screen').hide();
        $('header').show();
        $('nav').show();
        $('#page-content').show();
        $('footer').show();
        $('#sub-last').addClass('idle');
    }
});

Olos.agentChannelCommand.on('AgentAuthenticationChatWithPauseFail', function (agentId) {
    clearScreen();
    updateLog('<strong>agentAuthenticationChatWithPause fail:</strong> ' + JSON.stringify(agentId));
});

Olos.agentCommand.on('apiRequestError', function (obj) {
    updateLog('<strong>Erro na API:</strong> ' + JSON.stringify(obj));
}); 

Olos.agentCommand.on('loginccm', function (obj) {
    //implementação
    $('#user-name').text(obj.agentName);
    autoUpdateStatusTime();
    updateLog('<strong>loginccm:</strong> ' + JSON.stringify(obj));
});

Olos.agentCommand.on('logoutccm', function (obj) {
    //implementação
    Olos.agentChannelCommand.stop(); //parar a busca de eventos do chat
    updateLog('<strong>logoutccm:</strong> ' + JSON.stringify(obj));
});

Olos.agentCommand.on('logincampaign', function (obj) {
    //implementação
    updateLog('<strong>logincampaign</strong>: ' + JSON.stringify(obj));
});

Olos.agentCommand.on('listReasons', function (obj) {
    updateLog('<strong>listReasons</strong>: ' + JSON.stringify(obj));
    //implementação
});

Olos.agentCommand.on('listDispositions', function (obj) {
    updateLog('<strong>listDispositions</strong>: ' + JSON.stringify(obj));
});

Olos.agentCommand.on('logoutcampaign', function (obj) {
    //implementação
    updateLog('<strong>logoutcampaign:</strong> ' + JSON.stringify(obj));
});

Olos.agentCommand.on('changestatus', function (obj) {
    //implementação
    //Chamada da Função que Trata todos os status de Agente separadamente
    updateLog('<strong>changestatus:</strong> ' + JSON.stringify(obj));
    updateAgentStatus(obj.agentStatusId);

});

Olos.agentCommand.on('changestatusfail', function(obj) {
    //implementação
});

Olos.agentCommand.on('passcode', function (passcode) {
    //implementação
    updateLog('<strong>passcode:</strong> ' + passcode);
});

Olos.agentCommand.on('dispositionrequestfail', function (obj) {
    //implementação
});

Olos.agentCommand.on('screenpop', function (obj) {
    //implementação
    updateLog('<strong>screenpop:</strong> CALLID: ' + obj.callId + ' CampaignCode: ' + obj.campaignCode + ' CampaignData: ' + obj.campaignData + ' CampaignId: ' + obj.campaignId + ' CustomerId: ' + obj.customerId + ' PhoneNumber: ' + obj.phoneNumber + ' TableName: ' + obj.tableName);
    Olos.agentCommand.listDispositions(obj.campaignId);

    dadosCallData = obj.campaignData;
});


Olos.agentCommand.on('loginccmfail', function (obj) {
    //implementação
});

Olos.agentCommand.on('logincampaignfail', function (obj) {
    //implementação
});

Olos.agentCommand.on('logoutccmfail', function (obj) {
    //implementação
});

Olos.agentCommand.on('logoutcampaignfail', function (obj) {
    //implementação
});

Olos.agentCommand.on('onlinecampaignchangestatusid', function (obj) {
    //implementação
});

Olos.agentCommand.on('newchat', function (obj) {
    //implementação
});

Olos.agentCommand.on('newchatmsg', function (obj) {
    //implementação
});

Olos.agentCommand.on('endchat', function (obj) {
    //implementação
});

Olos.agentCommand.on('newmessage', function (obj) {
    //implementação
});

Olos.agentCommand.on('consultingrequestfail', function (obj) {
    updateLog('<strong>consultingrequestfail:</strong> ' + JSON.stringify(obj));
});

Olos.agentCommand.on('activecall', function (obj) {
    //implementação
    updateLog('<strong>activecall<strong>: CALLID: ' + obj);
});

Olos.agentCommand.on('manualcallrequestfail', function (obj) {
    updateLog('<strong>manualcallrequestfail:</strong> ' + JSON.stringify(obj));
});

Olos.agentCommand.on('changemanualcallstate', function (obj) {
    //implementação
    updateLog('<strong>changemanualcallstate:</strong> ' + JSON.stringify(obj));
});

Olos.agentCommand.on('RedialRequestFail', function (obj) {
    //implementação

});

Olos.agentCommand.on('redialsuccess', function (obj) {
    //implementação
});

Olos.agentCommand.on('listactivecalls', function (obj) {
    updateLog('<strong>listactivecalls:</strong> ' + JSON.stringify(obj));
});

Olos.agentCommand.on('privatecallbackfail', function (obj) {
    //implementação
});

Olos.agentCommand.on('thirdpartyscreenpop', function (obj) {
    //implementação
});

Olos.agentCommand.on('changepreviewcallstate', function (obj) {
    //implementação
});

Olos.agentCommand.on('changepreviewcallresult', function (obj) {
    //implementação
});

Olos.agentCommand.on('conferencerequestaccepted', function (msgObj) {
    updateLog('<strong>conferencerequestaccepted:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentCommand.on('conferencerequestfail', function (msgObj) {
    updateLog('<strong>conferencerequestfail:</strong> ' + JSON.stringify(msgObj));
});


Olos.agentCommand.on('participantjoinedconference', function (msgObj) {
    updateLog('<strong>participantjoinedconference:</strong> ' + JSON.stringify(msgObj));
});


Olos.agentCommand.on('participantleftconference', function (msgObj) {
    updateLog('<strong>participantleftconference:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentCommand.on('stopconferencerequestaccepted', function (msgObj) {
    updateLog('<strong>stopconferencerequestaccepted:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentCommand.on('stopconferencerequestfail', function (msgObj) {
    updateLog('<strong>stopconferencerequestfail:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentCommand.on('conferencepaused', function (msgObj) {
    updateLog('<strong>conferencepaused:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentCommand.on('conferenceresumed', function (msgObj) {
    updateLog('<strong>conferenceresumed:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentCommand.on('refercallrequestfail', function (msgObj) {
    updateLog('<strong>refercallrequestfail:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentCommand.on('GetMailingHistory', function (msgObj) {
    updateLog('<strong>GetMailingHistory:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentCommand.on('AgentDailySummary', function (msgObj) {
    console.log(msgObj);
    updateLog('<strong>[chat] - AgentDailySummary:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentCommand.on('getMySupervisor', function (msgObj) {
    console.log(msgObj);
    updateLog('<strong>GetMySupervisor:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentCommand.on('retrieveSupervisioningMessages', function (msgObj) {
    console.log(msgObj);
    updateLog('<strong>RetrieveSupervisioningMessages:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentCommand.on('pauserecordingresponse', function (msgObj) {
    console.log(msgObj);
    updateLog('<strong>pauserRcordingResponse :</strong> ' + JSON.stringify(msgObj));
});


Olos.agentCommand.on('resumerecordingresponse', function (msgObj) {
    console.log(msgObj);
    updateLog('<strong>ResumeRecordingResponse :</strong> ' + JSON.stringify(msgObj));
});
 

Olos.agentCommand.on('newmsgfromsupervisor', function (msgObj) {
    console.log(msgObj);
    updateLog('<strong>NewMsgFromSupervisor:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentCommand.on('sendmsgtosupervisorfail', function (msgObj) {
    console.log(msgObj);
    updateLog('<strong>SendMsgToSupervisorFail:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentCommand.on('listAvailableOnlineAgents', function (msgObj) {
    console.log(msgObj);
    updateLog('<strong>listAvailableOnlineAgents:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentCommand.on('extendwraptimeoutresponse', function (msgObj) {
    console.log(msgObj);
    updateLog('<strong>extendWrapTimeoutResponse:</strong> ' + JSON.stringify(msgObj));
});




//AGConferenceRequestAccepted
//AGConferenceRequestFail
//AGParticipantJoinedConference
//AGParticipantLeftConference
//AGStopConferenceRequestAccepted
//AGStopConferenceRequestFail
//AGConferencePaused
//AGConferenceResumed

Olos.mailingCommand.on('insertphonenumber', function (msgObj) {
    //implementação
});

Olos.mailingCommand.on('updateMailingData', function (msgObj) {
    updateLog('<strong>updateMailingData:</strong> ' + JSON.stringify(msgObj));
});

Olos.integrationCommand.on('listAvailableOnlineAgentsByCompany', function (obj) {

    updateLog('<strong>listAvailableOnlineAgentsByCompany:</strong> ' + JSON.stringify(obj));

});

Olos.integrationCommand.on('listCampaignsToConsulting', function (obj) {
    updateLog('<strong>listCampaignsToConsulting:</strong> ' + JSON.stringify(obj));
});


/* Region: Chat Events*/
Olos.agentChannelCommand.on('apiRequestError', function (obj) {
    updateLog('<strong>[chat] - Erro na API:</strong> ' + JSON.stringify(obj));
});


Olos.agentChannelCommand.on('changestatus', function(obj) {
     updateLog('<strong>[chat] - changestatus</strong> ' + JSON.stringify(obj));
     //updateAgentStatus(obj.agentStatusId);
 });

 Olos.agentChannelCommand.on('loginresponse', function(obj) {
    updateLog('<strong>[chat] - loginresponse</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('logoutresponse', function(obj) {
    updateLog('<strong>[chat] - logoutresponse</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentCommand.on('logoutreason', function (msgObj) {
    updateLog('<strong>LogoutReason:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentChannelCommand.on('logincampaign', function(obj) {
    updateLog('<strong>[chat] - logincampaign</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('logoutcampaign', function(obj) {
    updateLog('<strong>[chat] - logoutcampaign</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('screenpop', function(obj) {
    updateLog('<strong>[chat] - screenpop</strong> ' + JSON.stringify(obj));
    Olos.agentChannelCommand.listChatDispositions(obj.campaignId);
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('messageresponse', function(obj) {
    updateLog('<strong>[chat] - messageresponse</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('messagereceived', function(obj) {
    updateLog('<strong>[chat] - messagececeived</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('typing', function(obj) {
    updateLog('<strong>[chat] - typing</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('inactivityalert', function(obj) {
    updateLog('<strong>[chat] - inactivityalert</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('pauseresponse', function(obj) {
    updateLog('<strong>[chat] - pauseresponse</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('quitpauseresponse', function(obj) {
    updateLog('<strong>[chat] - quitpauseresponse</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('dispositionresponse', function(obj) {
    updateLog('<strong>[chat] - dispositionresponse</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('listpausereasonsresponse', function(obj) {
    updateLog('<strong>[chat] - listpausereasonsresponse</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('dialogterminated', function(obj) {
    updateLog('<strong>[chat] - dialogterminated</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('currentdialogsresponse', function(obj) {
    updateLog('<strong>[chat] - currentdialogsresponse</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('heartbeat', function(obj) {
    updateLog('<strong>[chat] - heartbeate</strong> ' + JSON.stringify(obj));
    //updateAgentStatus(obj.agentStatusId);
});

Olos.agentChannelCommand.on('listdispositionresponse', function (obj) {
    updateLog('<strong>[chat] - listdispositionresponse</strong>: ' + JSON.stringify(obj));
});

Olos.agentChannelCommand.on('resetinactivitytimerresponse', function (obj) {
    updateLog('<strong>[chat] - resetinactivitytimerresponse</strong>: ' + JSON.stringify(obj));
});

Olos.agentChannelCommand.on('GetMailingHistory', function (msgObj) {
    updateLog('<strong>[chat] - GetMailingHistory:</strong> ' + JSON.stringify(msgObj));
});

Olos.agentChannelCommand.on('AgentDailySummary', function (msgObj) {
    updateLog('<strong>[chat] - AgentDailySummary:</strong> ' + JSON.stringify(msgObj));
});



/* End Region: Channel Events*/

Olos.userConfig.on('getchatsupervisoragent', function (msgObj) {
    console.log(JSON.stringify(msgObj[0]));
    updateLog('<strong>getChatSupervisorAgent:</strong> ' + JSON.stringify(msgObj[0]));
});


/**
 * Trata os eventos recebidos do Olos
 * @param {ObjEvent} objEvent
 * @returns {undefined}
 * ==== DEPRECATED ====
 */
function olosEvents(objEvent) { } // manter para compatibilidade da versão anterior

//Resgatando a lista de Status do Agente com Descrições em Português
var listAgentStatus = Olos.agentCommand.getListStatusAgent();

//Resgatando a lista de Status do Agente do Chat com Descrições em Português
var listAgentChannelStatus = Olos.agentChannelCommand.getListStatusAgent();

/**
 * Atualiza e Trata os status do agente
 * @param agentStatusId
 * @returns {undefined}
 * obs: 
 */
function updateAgentStatus(agentStatusId) {

    switch (agentStatusId) {
        case listAgentStatus.Idle.value:
            $('#sub-last').removeClass().addClass('idle');
            $('#last').removeClass('wraptmo').addClass('last');
            break;
        case listAgentStatus.Talking.value:
            $('#sub-last').removeClass().addClass('talking');
            break;
        case listAgentStatus.Wrap.value:
            {
                $('#lb-agent-status').text(listAgentStatus.Wrap.description);
                $('#sub-last').removeClass().addClass('wrap');
                $('#last').removeClass('last').addClass('wraptmo').animate({
                    opacity: 1
                }, 1000);
            }
            break;
        case listAgentStatus.WrapWithPause.value:
            {
                $('#sub-last').removeClass().addClass('wrap');
                $('#last').removeClass('last').addClass('wraptmo').animate({
                    opacity: 1
                }, 1000);
            }
            break;
        case listAgentStatus.WrapWithPrivateCallback.value:
            {
                $('#sub-last').removeClass().addClass('wrap');
                $('#last').removeClass('last').addClass('wraptmo').animate({
                    opacity: 1
                }, 1000);
            }
            break;
        case listAgentStatus.WrapWithManualCall.value:
            {
                $('#sub-last').removeClass().addClass('wrap');
                $('#last').removeClass('last').addClass('wraptmo').animate({
                    opacity: 1
                }, 1000);
            }
            break;
        case listAgentStatus.WrapWithEnding.value:
            {
                $('#sub-last').removeClass().addClass('wrap');
                $('#last').removeClass('last').addClass('wraptmo').animate({
                    opacity: 1
                }, 1000);
            }
            break;
        case listAgentStatus.PrivateCallback.value:
            {
                $('#sub-last').removeClass().addClass('private-cb');
                $('#last').removeClass('wraptmo').addClass('last');
            }
            break;
        case listAgentStatus.Redial.value:
            {
                $('#sub-last').removeClass().addClass('private-cb');
                $('#last').removeClass('wraptmo').addClass('last');
            }
            break;
        case listAgentStatus.Pause.value:
            {
                $('#sub-last').removeClass().addClass('pause');
                $('#last').removeClass('wraptmo').addClass('last');
            }
            break;
        case listAgentStatus.ManualCall.value:
            {
                $('#sub-last').removeClass().addClass('manual');
                $('#last').removeClass('wraptmo').addClass('last');
            }
            break;
        case listAgentStatus.Consulting.value:
            {
                $('#sub-last').removeClass().addClass('consult');
            }
            break;

        case listAgentStatus.InConference.value:
        {
            $('#sub-last').removeClass().addClass('conference');
            $('#lb-agent-status').text(listAgentStatus.InConference.description);
        }
        break;
        case listAgentStatus.InConferenceWithEnding.value:
        {
            $('#sub-last').removeClass().addClass('conference');
            $('#lb-agent-status').text(listAgentStatus.InConferenceWithEnding.description);
        }
        break;
        case listAgentStatus.InConferenceWithManualCall.value:
        {
            $('#sub-last').removeClass().addClass('conference');
            $('#lb-agent-status').text(listAgentStatus.InConferenceWithManualCall.description);
        }
        break;
        case listAgentStatus.InConferenceWithPause.value:
        {
            $('#sub-last').removeClass().addClass('conference');
            $('#lb-agent-status').text(listAgentStatus.InConferenceWithPause.description);
        }
        break;
        case listAgentStatus.InConferenceWithPersonalCall.value:
        {
            $('#sub-last').removeClass().addClass('conference');
            $('#lb-agent-status').text(listAgentStatus.InConferenceWithPersonalCall.description);
        }
        break;
        case listAgentStatus.InConferenceWithPrivateCallback.value:
        {
            $('#sub-last').removeClass().addClass('conference');
            $('#lb-agent-status').text(listAgentStatus.InConferenceWithPrivateCallback.description);
        }
        break;
        case listAgentStatus.Holding.value:
            {
                $('#sub-last').removeClass().addClass('manual');
            }
            break;
        case listAgentStatus.Ending.value:
            {
                $('#sub-last').removeClass().addClass('ending');
                $('#last').removeClass('wraptmo').addClass('last');
                //clearTimer();
            }
            break;
        case listAgentStatus.End.value:
            {
                $('#sub-last').removeClass().addClass('ending');
                $('#last').removeClass('wraptmo').addClass('last');
                //clearTimer();
            }
            break;

    }

    $('#lb-agent-status').text(listAgentStatus[agentStatusId].description)
    startStatusTime();
}
/**
 * Atualiza status da chamada manual
 * @param {ObjManualCallState} objManualCallState objeto da estado da chamada manual (status: Started, CustomerConnected, Finished)
 * @returns {undefined}
 */
function updateManualCallStatus(objManualCallState) {
    switch (objManualCallState.callState) {
        case enumCallStatus.STARTED.value:
            {
                $('#lb-agent-status').text(enumCallStatus.STARTED.value);
            }
            break;
        case enumCallStatus.CUSTOMER_CONNECTED.value:
            {
                $('#lb-agent-status').text(enumCallStatus.CUSTOMER_CONNECTED.value);
            }
            break;
        case enumCallStatus.FINISHED.value:
            {
                $('#lb-agent-status').text(enumCallStatus.FINISHED.value);
            }
            break;
    }
}


/** Funções  de Auxilio para construção da Tela de Testes */
function updateLog(logMsg) {
    var log = document.getElementById('waiting-text');
    log.innerHTML = log.innerHTML + " \n " + logMsg + "<br />";
}

function clearLog() {
    var log = document.getElementById('waiting-text');
    log.innerHTML = '';
}

var segundo = 0;
var minuto = 0;
var hora = 0;
var timer;

function startStatusTime() {
    segundo = 0;
    minuto = 0;
    hora = 0;
}

var updateStatusTime = function () {
    if (segundo <= 58) {
        segundo++;
    } else if (segundo === 59 && minuto <= 59) {
        segundo = 0;
        minuto++;
    } else if (minuto === 59) {
        minuto = 0;
        hora++;
    } else if (hora === 23) {
        hora = 0;
    }

    if (segundo < 10 && minuto < 10) {
        $('#lb-clock').text("0" + hora + ":0" + minuto + ":0" + segundo);
    } else if (segundo > 9 && minuto < 10) {
        $('#lb-clock').text("0" + hora + ":0" + minuto + ":" + segundo);
    } else if (segundo < 10 && minuto > 9) {
        $('#lb-clock').text("0" + hora + ":" + minuto + ":0" + segundo);
    }
    autoUpdateStatusTime();
};

function autoUpdateStatusTime() {
    timer = setTimeout(updateStatusTime, 1000);
}

function clearTimer() {
    clearTimeout(timer);
}